<?php
class User_model extends CI_Model
{
	public function validate()
	{
		$this->db->where('email', $this->input->post('email'));
		$this->db->where('password', md5($this->input->post('password')));
		$query= $this->db->get('users');
	
		if($query->num_rows() == 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function validate_password($user_id, $password)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('password', md5($password));
		$query= $this->db->get('users');
	
		if($query->num_rows())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_id($data)
	{
		foreach ($data as $key => $value) {
			$this->db->where($key, $value);
		}
		$query= $this->db->get('users');
		
		if($query->num_rows() == 1)
		{
			return $query->row()->user_id;
		}
		else
		{
			return 0;
		}
	}

	public function count_all()
	{
		return $this->db->count_all('users');
	}

	public function get_data_mine($include_photo = TRUE)
	{
		if($this->session->userdata('is_user_logged_in') || $this->session->userdata('is_admin_logged_in'))
		{
			return $this->user_model->get_data($this->session->userdata('user_id'), $include_photo);
		}
	}

	public function get_data($user_id, $include_photo = TRUE, $include_password = FALSE)
	{
		$select = 'user_id, name, username, email, work, birthday, relationship, create_date';
		if($include_password)
		{
			$select = 'user_id, name, username, email, work, birthday, relationship, create_date, password';
		}
		$this->db->select($select);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('users');
		if($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				if($include_photo)
				{
					$row['filename'] = $this->user_model->get_photo($row['user_id']);
				}
				return $row;
			}
			
		}
		else
		{
			return array();
		}
	}

	public function get_data_many($limit, $offset = 0, $include_photo = TRUE, $include_mine = FALSE)
	{
		$this->db->select('user_id, name, username, email, work, birthday, relationship');
		$this->db->order_by('create_date', 'desc');
		if(!$include_mine)
		{
			$this->db->where('user_id !=', $this->session->userdata('user_id'));
		}

		$query= $this->db->get('users', $limit, $offset);
		
		$data = array();
		if($query->num_rows())
		{
			foreach ($query->result_array() as $row) {
				if($include_photo)
				{
					$row['filename'] = $this->user_model->get_photo($row['user_id']);
				}
				array_push($data, $row);	
			}
			return $data;
		}
		else
		{
			return array();
		}
		
	}

	public function get_photo($user_id)
	{
		$query = $this->db->query("SELECT filename FROM users NATURAL JOIN images WHERE users.user_id = ".$user_id);
		return $query->row()->filename;
	}
	
	public function add($user){
		$query = $this->db->insert('users', $user);
		if($query)
		{
			return TRUE;
		}
		else
		{
			return FALSE;	
		}
	}

	public function update($user, $user_id){
		if($this->session->userdata('is_user_logged_in') || $this->session->userdata('is_admin_logged_in'))
		{	
			$this->db->where('user_id', $user_id);
			$query = $this->db->update('users', $user);

			if($query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;	
			}
		}
	}

	public function delete($user_id){
		if($this->session->userdata('is_admin_logged_in'))
		{
			$this->db->where('user_id', $user_id);
			$query = $this->db->delete('users');
			if($query)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}

	public function search($q, $include_photo = TRUE)
	{
		if($this->session->userdata('is_user_logged_in') || $this->session->userdata('is_admin_logged_in'))
		{
			$this->db->select('user_id, name, username, email');
			$this->db->where("(name LIKE '%".$q."%' OR username LIKE '%".$q."%' OR email LIKE '%".$q."%')", NULL, FALSE);
			$query= $this->db->get('users');
			
			$data = array();
			if($query->num_rows())
			{
				foreach ($query->result_array() as $row) {
					if($include_photo)
					{
						$row['filename'] = $this->user_model->get_photo($row['user_id']);
					}
					array_push($data, $row);	
				}
				return $data;
			}
			else
			{
				return array();
			}
		}
	}
}

?>