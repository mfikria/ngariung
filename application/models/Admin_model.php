<?php
class Admin_model extends CI_Model
{
	public function validate()
	{
		$this->db->where('adminname', $this->input->post('adminname'));
		$this->db->where('password', md5($this->input->post('password')));
		$query= $this->db->get('admins');
	
		if($query->num_rows() == 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_id($data)
	{
		foreach ($data as $key => $value) {
			$this->db->where($key, $value);
		}
		$query= $this->db->get('admins');
		
		if($query->num_rows() == 1)
		{
			return $query->row()->admin_id;
		}
		else
		{
			return 0;
		}
	}

	public function get_photo($admin_id)
	{
		$query = $this->db->query("SELECT filename FROM admins NATURAL JOIN images WHERE admins.admin_id = ".$admin_id);
		return $query->row()->filename;
	}

	public function get_data($admin_id, $include_photo = TRUE)
	{
		$this->db->select('admin_id, name, adminname');
		$this->db->where('admin_id', $admin_id);
		$query = $this->db->get('admins');
		if($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				if($include_photo)
				{
					$row['filename'] = $this->admin_model->get_photo($row['admin_id']);
				}
				return $row;
			}
			
		}
		else
		{
			return array();
		}
	}
}

?>