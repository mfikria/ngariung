<!--Start Panel-->
<div class="panel panel-default">
<div class="panel-body padding-top padding-top-zero padding-right-zero padding-left-zero">
<div class="media media-visible pd-right">
<a class="pull-left photo-card-live myprofile">
<img src="{img_user}" alt="Image" class="border-image-profile img-rounded photo-card">
</a>
<div class="media-body col-md-12">
<h1 class="user-name-profile-card">
<a href="" class="myprofile">
<span class="word-brk">{name}</span>
</a>                  
</h1>
<p class="text-col">
<small>@{username}</small>
</p>  
</div>
</div>
</div><!--Panel Body-->
<!--Panel Body-->
<div class="form-group">
<span class="fa fa-gear myicon-right col-md-2"></span>
<div class="col-md-10">
<small>{email}</small>
</div>
</div>

<div class="form-group">
<span class="fa fa-heart myicon-right col-md-2"></span>
<div class="col-md-10">
<small>{relationship}</small>
</div>
</div>

<div class="form-group">
<span class="fa fa-briefcase myicon-right col-md-2"></span>
<div class="col-md-10">
<small>{work}</small>
</div>
</div>

<div class="form-group">
<span class="fa fa-birthday-cake myicon-right col-md-2"></span>
<div class="col-md-10">
<small>{birthday}</small>
</div>
</div>
</div><!--Panel Default--> 
<div class="col-md-12">
<a href="<?php echo base_url();?>profile" class="btn btn-block btn-lg btn-info">See your profile <span class="fui-arrow-right"></span> </a>
</div>    