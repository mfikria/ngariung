<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="description" content="Mini Social Media">
<meta name="keywords" content="social,media">
<link rel="icon" href="<?php echo base_url('assets/img')?>/logo.ico">

<title>Ngariung - Mini Social Media</title>

<!--Included CSS & JS-->
<link href="<?php echo base_url('assets/css')?>/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css')?>/main.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css')?>/flat-ui.css" rel="stylesheet">
<link href="<?php echo base_url('assets/css')?>/font-awesome.min.css" rel="stylesheet">
<script src="<?php echo base_url('assets/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/js')?>/jquery-ui-1.10.3.min.js"></script>
<script src="<?php echo base_url('assets/js')?>/bootstrap.min.js"></script>