
<!--USER PROFILE-->
<li class="media border-group hoverList li-group list-group-item"> 
<div class="media li-group">
<div class="pull-left">
<img src="{img_user}" alt="Avatar" class="media-object img-rounded">
</div>
<div class="media-body clearfix text-overflow">
<strong class="media-heading">
<a href="" class="openModal username-title-2">
{name}
</a>
<small style="color: #999;">@{username}</small>
</strong>
<p class="text-col">                          
<small>{email}</small>
</p>
</div>
</div>
</li>

