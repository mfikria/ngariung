<!--navigation_bar-->  
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="container row col-md-12">

<div class="col-md-1">
<a class="navbar-brand" href="<?php echo base_url()?>">
<img src="<?php echo base_url('assets/img')?>/logo.png" class="logo">
</a>
</div><!--end of navbar-header-->  

<!--collapse-->  
<div class="col-md-11">
<form class="navbar-form navbar-left" action="search">
<div class="form-group"><div class="input-group">
<input class="form-control" id="navbarInput" name="q" type="search" placeholder="Search">
<span class="input-group-btn">
<button type="submit" class="btn"><span class="fui-search"></span></button>
</span>
</div>
</div>
</form>
<ul class="nav navbar-nav navbar-right topbar">
<li class="dropdown">
<a href="" class="userAvatar myprofile">
<img src="{img_user}" alt="User" class="img-rounded" width="24" height="24"> 
<span class="title-dropdown">{name}</span>
</a> 
</li>


<li class="dropdown">
<a class="dropdown-toggle" id="dropdownMenu" data-toggle="dropdown" href="javascript:void(0);">
<i class="fa fa-gear myicon-right"></i> <i class="fa fa-caret-down"></i>
</a> 

<ul class="dropdown-menu dropdown-settings arrow-up-user">
<li><a href="" class="myprofile"><i class="fa fa-user myicon-right"></i> My Profile</a></li>
<li><a href="<?php echo base_url('setting')?>"><i class="fa fa-gear myicon-right"></i> Settings</a></li>
<li class="divider"></li>
<li class="bottomList"><a href="<?php echo base_url('logout')?>" class="logout"><i class="fa fa-close myicon-right"></i> Log out</a></li>
</ul>
</li><!--End Settings-->
</ul><!--End of navbar-nav-->
</div><!--End of nav-collapse-->
</div><!--End of container-->
</div><!--End of NaBar-->