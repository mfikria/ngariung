<!DOCTYPE html>

<html lang="en">
	<head>
		<?php $this->load->view('template/head');?>
	</head>

  <body class="welcomepage">

    <!--navigation_bar-->  
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="row col-pb">
				<div class="col-md-2">
					<a class="navbar-brand" href="<?php echo base_url()?>">
						<img src="<?php echo base_url('assets/img')?>/logo.png" class="logo">
					</a>
				</div><!--end of navbar-header-->  

				<!--collapse-->  
				<div class="col-md-10">
				
					<ul class="nav navbar-nav navbar-right topbar">


						<li class="dropdown">
							<a class="dropdown-toggle" id="dropdownMenu" data-toggle="dropdown" href="javascript:void(0);">
								<i class="fa fa-sign-in myicon-right"></i> Login <i class="fa fa-caret-down"></i>
							</a> 

							<ul class="dropdown-menu dropdown-settings arrow-up-user">
								<div class="login-form">
						          <?php
						          	echo form_open('/user/login_validation'); 
									
									echo '<div class="form-group">';
									echo form_input(array('class' => 'form-control login-field', 'name' => 'email', 'placeholder' => 'email', 'value' => $this->input->post['email']));
									echo '<label class="login-field-icon fui-mail"></label></div>';

									echo '<div class="form-group">';
									echo form_password(array('class' => 'form-control login-field', 'name' => 'password', 'placeholder' => 'password'));
									echo '<label class="login-field-icon fui-lock"></label></div>';

									echo '<div class="form-group">';
									echo form_submit(array('id' => 'buttonSignIn', 'class' => 'btn btn-primary btn-lg btn-block'), 'Login');
									echo '</div>';

									echo form_close();
						          ?>
						          </div><!-- login-form -->
							</ul>
						</li><!--End Settings-->
					</ul><!--End of navbar-nav-->
				</div><!--End of nav-collapse-->
			</div><!--End of container-->
		</div><!--End of NaBar-->
	          		
					          	
	        
    
    <!-- container -->
    <div class="container container-main">
    	<!-- col-md -->
    	<div class="col-md-8 col-pb">
    		<?php 
	    		if(!empty($this->session->flashdata('confirm')['success'])){
			    	echo '<div class="alert alert-success" id="success_signin" role="alert">'.$this->session->flashdata('confirm')['success'].'</div>';
			  	}
			  	if(!empty($this->session->flashdata('confirm')['error'])){
			    	echo '<div class="alert alert-danger" id="error" role="error">'.$this->session->flashdata('confirm')['error'].'</div>';
			  	}
  			?>
    		<div class="imglogo">
    			<img src="<?php echo base_url('assets/img') ?>/logo_ngariung-01.png" class="imglogo-item">
    		</div>
    			<h6 class="sub-title-home">
    				Share and connect with your loved ones anywhere and anytime! Sign Up now!			
    	</div><!--/col-md-8 -->
    	
      <!-- col-md -->
      <div class="col-md-4">
      
        
        <!-- Start Login Form -->
      	<div class="panel panel-default panel-login">
          <div class="login-form">
          	<?php
	          	echo form_open('/user/signup_validation'); 
				
				echo '<div class="form-group">';
				echo form_input(array('class' => 'form-control login-field', 'name' => 'name', 'placeholder' => 'name', 'value' => $this->input->post['name']));
				echo '<label class="login-field-icon fui-user"></label></div>';

				echo '<div class="form-group">';
				echo form_input(array('class' => 'form-control login-field', 'name' => 'username', 'placeholder' => 'username', 'value' => $this->input->post['username']));
				echo '<label class="login-field-icon fui-user"></label></div>';

				echo '<div class="form-group">';
				echo form_password(array('class' => 'form-control login-field', 'name' => 'password', 'placeholder' => 'password'));
				echo '<label class="login-field-icon fui-lock"></label></div>';

				echo '<div class="form-group">';
				echo form_password(array('class' => 'form-control login-field', 'name' => 'cpassword', 'placeholder' => 'confirm password'));
				echo '<label class="login-field-icon fui-lock"></label></div>';

				echo '<div class="form-group">';
				echo form_input(array('class' => 'form-control login-field', 'name' => 'email', 'placeholder' => 'email', 'value' => $this->input->post['email']));
				echo '<label class="login-field-icon fui-mail"></label></div>';

				$options = array(
				        'single'         => 'Single',
				        'inrelationship' => 'In relationship',
				        'engaged'        => 'Engaged',
				        'married'        => 'Married',
				);
				echo '<div class="form-group form-control">';
				echo form_dropdown('relationship', $options, set_value('relationship'));
				echo '<label class="login-field-icon fui-heart"></label></div>';

			
				echo '<div class="form-group">';
				echo form_submit(array('id' => 'buttonSubmit', 'class' => 'btn btn-primary btn-lg btn-block btn-success'), 'Sign Up');
				echo '</div>';

				echo form_close();
	        ?>
          </div>
          <!--./form -->
        </div><!-- /End Login Form -->
        
                
      </div><!--/col-md-4 -->
      <footer class="clearfix footer-grid">
        <p class="footer-p">
    		<a class="link-footer" href="">About Us</a> 
    		<a class="link-footer" href="">Help</a>
    		<a class="link-footer" href="">Terms of Service</a>

	    	© 2015 Ngariung
	    </p>
      </footer>
      
    </div><!-- /.container -->
	</body>
</html>