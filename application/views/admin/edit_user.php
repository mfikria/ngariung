<!DOCTYPE html>

<html>
  <head>
    <?php $this->load->view('template/head');?>
    
  </head>

  <body>
    <?php echo $this->load->view('admin/template/navigation_bar', $admin, TRUE)?>
    <div class="container">
      <div class="row col-pb">
          <div class="panel panel-default col-md-12">
          <div class="panel-heading grid-panel-title row col-md-12">
            <div class="col-sm-9">
              <h2 class="panel-title titleBar" data-title="Profile">Edit User</h2>
            </div>
            <div class="clearfix visible-xs"></div>
        

          </div>
    
          <div class="panel-body col-md-12">
            <?php
		      	echo form_open(base_url('admin/update_user').'/'.$user['user_id'], array('class'=> 'form-horizontal')); 
					
					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Username</label><div class="col-sm-10">';
					echo form_input(array('id' => 'usernameEdit', 'class' => 'input-sm', 'name' => 'username', 'placeholder' => 'Username'), $user['username']);
					echo '</div></div>';

					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Email</label><div class="col-sm-10">';
					echo form_input(array('id' => 'emailEdit', 'class' => 'input-sm', 'name' => 'email', 'placeholder' => 'Email'), $user['email']);
					echo '</div></div>';
					
					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Name</label><div class="col-sm-10">';
					echo form_input(array('id' => 'nameEdit', 'class' => 'form-control input-sm', 'name' => 'name', 'placeholder' => 'Name'), $user['name']);
					echo '</div></div>';

					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Work</label><div class="col-sm-10">';
					echo form_input(array('id' => 'workEdit', 'class' => 'form-control input-sm', 'name' => 'work', 'placeholder' => 'Work'), $user['work']);
					echo '</div></div>';

					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Birthday</label><div class="col-sm-10">';
					echo form_input(array('id' => 'birthdayEdit', 'class' => 'form-control input-sm', 'name' => 'birthday', 'placeholder' => 'DD-MM-YYYY'),  $user['birthday']);
					echo '</div></div>';

					$options = array(
					        'single'         => 'Single',
					        'inrelationship' => 'In relationship',
					        'engaged'        => 'Engaged',
					        'married'        => 'Married',
					);
					echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Relationship</label><div class="col-sm-10">';
					echo form_dropdown(array('id' => 'realationshipEdit', 'class' => 'input-sm btn-block', 'name' => 'relationship'), $options, $user['relationship']);
					echo '</div></div>';

					echo '<div class="alert alert-danger" id="error">'.validation_errors().'</div>';

					echo '<div class="form-group"><div class="col-sm-offset-2 col-sm-10">';
					echo form_submit(array('id' => 'editProfile', 'class' => 'btn btn-info btn-sm profile-settings'), 'Save');
					echo '</div></div>';

					echo form_close();
	  		?>	
          </div><!--Panel Body-->

          </div><!--Panel Default-->
        
      </div><!--End of row -->
    </div> <!--End of container -->
  </body>
</html>