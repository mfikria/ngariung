<!--navigation_bar-->  
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="container">

<div class="navbar-header">
<a class="navbar-brand" href="<?php echo base_url('admin')?>">
<img src="<?php echo base_url('assets/img')?>/logo.png" class="logo">
</a>
</div><!--end of navbar-header-->  

<!--collapse-->  
<div class="collapse navbar-collapse">

<ul class="nav navbar-nav navbar-right topbar">
<li class="dropdown">
<a href="" class="userAvatar myprofile">
<img src="<?php echo base_url('assets/img/profile').'/'.$filename;?>" alt="User" class="img-rounded" width="24" height="24"> 
<span class="title-dropdown"><?php echo $name;?></span>
</a> 
</li>


<li class="dropdown">
<a class="dropdown-toggle" id="dropdownMenu" data-toggle="dropdown" href="javascript:void(0);">
<i class="fa fa-gear myicon-right"></i> <i class="fa fa-caret-down"></i>
</a> 

<ul class="dropdown-menu dropdown-settings arrow-up-user">
<li><a href="<?php echo base_url('admin')?>/add_user"><i class="fa fa-plus myicon-right"></i>Add user</a></li>
<li class="divider"></li>
<li class="bottomList"><a href="<?php echo base_url('logout')?>" class="logout"><i class="fa fa-close myicon-right"></i> Log out</a></li>
</ul>
</li><!--End Settings-->
</ul><!--End of navbar-nav-->
</div><!--End of nav-collapse-->
</div><!--End of container-->
</div><!--End of NaBar-->