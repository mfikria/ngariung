<!DOCTYPE html>

<html>
	<head>
		<?php $this->load->view('template/head');?>
	</head>
	<body>
	<div class="container">

		<div class="form_login">
		<div class="col-md-12">
			<h2>Login Administrator</h2>
			<?php
	          	echo form_open('/admin/login_validation', array('class' => 'form-horizontal')); 
				
				echo '<div class="form-group"><label class="col-md-1 login-field-icon fui-user"></label>';
				echo form_input(array('class' => 'col-md-11 form-control login-field', 'name' => 'adminname', 'placeholder' => 'adminname', 'value' => $this->input->post['adminname']));
				echo '</div>';

				echo '<div class="form-group"><label class="col-md-1 login-field-icon fui-lock"></label>';
				echo form_password(array('class' => 'col-md-11 form-control login-field', 'name' => 'password', 'placeholder' => 'password'));
				echo '</div>';

				echo '<div class="form-group">';
				echo form_submit(array('id' => 'buttonSignIn', 'class' => 'btn btn-primary col-md-12 btn-lg btn-block'), 'Login');
				echo '</div>';

				echo form_close();
	          ?>
	          </div>
          </div>
		</div>
		
	</body>
</html>