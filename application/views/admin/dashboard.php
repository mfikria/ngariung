<!DOCTYPE html>

<html>
  <head>
    <?php $this->load->view('template/head');?>
    
  </head>

  <body>
    <?php echo $this->load->view('admin/template/navigation_bar', $admin, TRUE)?>
    <div class="container">
      <div class="row col-pb">
         <?php 
          if(!empty($this->session->flashdata('confirm')['success'])){
            echo '<div class="alert alert-success" id="success_signin" role="alert">'.$this->session->flashdata('confirm')['success'].'</div>';
          }
          if(!empty($this->session->flashdata('confirm')['error'])){
            echo '<div class="alert alert-danger" id="error" role="error">'.$this->session->flashdata('confirm')['error'].'</div>';
          }
        ?>
          <div class="panel panel-default col-md-12">
          <div class="panel-heading grid-panel-title row col-md-12">
            <div class="col-sm-9">
              <h2 class="panel-title titleBar" data-title="Profile">Users Management</h2>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-sm-3">
              <a href="<?php echo base_url('admin')?>/add_user" class="btn btn-block btn-sm btn-warning float-right">Add User  <span class="fui-plus-circle"></span></a>
            </div>

          </div>
    
          <div class="panel-body">
          <div class="tablewrap col-md-12">
            <?php
              $tmpl = array (
                    'table_open'          => '<table class="table-striped  table-condensed table-responsive xsmall">',

                    'table_close'         => '</table>'
              );

              $this->table->set_template($tmpl);

              $this->table->set_heading('','Name', 'Username', 'Email', 'Work', 'Birthday', 'Relationship');

             
              foreach($users as $user)
              {
                $button ='<div class="btn-group">
                          <a class="btn btn-xs btn-primary btn-warning" href="'.base_url('admin/edit_user').'/'.$user['user_id'].'"><span class="fui-new"></span></a>
                          <a class="btn btn-xs btn-primary btn-danger" href="'.base_url('admin/delete_user').'/'.$user['user_id'].'"><span class="fui-cross-circle"></span></a>
                          </div>';

                $this->table->add_row($button, $user['name'], $user['username'], $user['email'], $user['work'], $user['birthday'], $user['relationship']);
              }
              
              echo $this->table->generate();
            ?>
            </div>
            <div class="col-xs-2"></div>
            <div class="pagination col-xs-8">
              <ul>
                <?php echo $this->pagination->create_links();?>
              </ul>
            </div>
            <div class="col-xs-2"></div>
            
          </div><!--Panel Body-->

          </div><!--Panel Default-->
        
      </div><!--End of row -->
    </div> <!--End of container -->
  </body>
</html>