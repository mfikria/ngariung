<!DOCTYPE html>
<html>
	<head>
		<?php $this->load->view('template/head');?>
		<script>
			//function for settings menu tab
			$(document).ready(function(){
				
				$('.tabs li').click(function(){
					var tab_id = $(this).attr('data-tab');

					$('.tabs li').removeClass('current');
					$('.tab-content').removeClass('current');

					$(this).addClass('current');
					$("#"+tab_id).addClass('current');
				})

			})
		</script>
	</head>
	<body>
		<?php echo $this->parser->parse('template/navigation_bar', array('img_user' => base_url('assets/img/profile').'/'.$filename, 'name' => $name), TRUE)?>

		<!--container-->  
    	<div class="container col-md-12">
	    	<div class="row col-pb"> 
				<div class="col-md-4">
		   			<?php echo $this->parser->parse('template/profile_banner', array('name' => $name,
                                                                    'username' => $username,
                                                                    'email' => $email,
                                                                    'work' => $work,
                                                                    'relationship' => $relationship,
                                                                    'birthday' => $birthday,
                                                                    'img_user' => base_url('assets/img/profile').'/'.$filename
                                                                    ), TRUE);
		   				$this->load->view('template/setting_navigation');
                    ?>
				</div><!--end of col-md-*-->
 			
 				<div class="col-md-8">
	    		<?php 
		    		if(!empty($this->session->flashdata('confirm')['success'])){
				    	echo '<div class="alert alert-success" id="success_signin" role="alert">'.$this->session->flashdata('confirm')['success'].'</div>';
				  	}
				  	if(!empty($this->session->flashdata('confirm')['error'])){
				    	echo '<div class="alert alert-danger" id="error" role="error">'.$this->session->flashdata('confirm')['error'].'</div>';
				  	}
	  			?>
			     	<div id="tab-1" class="panel panel-default current tab-content">
						<div class="panel-heading grid-panel-title">
							<h3 class="panel-title titleBar" data-title="Profile">Setting</h3>
						</div>
		  
						<div class="panel-body">
							<?php
						      	echo form_open('/user/update_setting', array('class' => 'form-horizontal')); 
									
									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Username</label><div class="col-sm-10">';
									echo form_input(array('id' => 'usernameEdit', 'class' => 'form-control input-sm', 'name' => 'username', 'placeholder' => 'Username'), $username);
									echo '</div></div>';

									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Email</label><div class="col-sm-10">';
									echo form_input(array('id' => 'emailEdit', 'class' => 'form-control input-sm', 'name' => 'email', 'placeholder' => 'Email'), $email);
									echo '</div></div>';
								
									echo '<div class="alert alert-danger" id="error">'.validation_errors().'</div>';

									echo '<div class="form-group"><div class="col-sm-offset-2 col-sm-10">';
									echo form_submit(array('id' => 'editProfile', 'class' => 'btn btn-info btn-sm profile-settings'), 'Save');
									echo '</div></div>';

									echo form_close();
					  		?>	  
						</div><!--Panel Body-->

   					</div><!--Panel Default-->

			     	<div id="tab-2" class="panel panel-default tab-content">
						<div class="panel-heading grid-panel-title">
							<h3 class="panel-title titleBar" data-title="Profile">Profile</h3>
						</div>
		  
						<div class="panel-body">
						<div class="row form-group">
			    <div class="clearfix visible-xs-block"></div>
			    <div class="col-md-12">
			    	
	  <div class="labelAvatar col-md-3">
	  <img src="<?php echo base_url('assets/img/profile').'/'.$filename?>" alt="Image" class="border-image-profile img-rounded photo-card">

		
	    				</div>
			
		<div class="col-md-9">
		  	<?php echo form_open_multipart('uploader/do_upload');?>

				<input type="file" name="userfile" class="col-sm-12" />

				<input type="submit" value="upload" class="col-sm-12"/>

			<?php echo form_close();?>
			</div>

			</div>
			</div>
			<div class="row">
							<?php
						      	echo form_open('/user/update_profile', array('class' => 'form-horizontal')); 
									
									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Name</label><div class="col-sm-10">';
									echo form_input(array('id' => 'nameEdit', 'class' => 'form-control input-sm', 'name' => 'name', 'placeholder' => 'Name'), $name);
									echo '</div></div>';

									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Work</label><div class="col-sm-10">';
									echo form_input(array('id' => 'workEdit', 'class' => 'form-control input-sm', 'name' => 'work', 'placeholder' => 'Work'), $work);
									echo '</div></div>';

									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Birthday</label><div class="col-sm-10">';
									echo form_input(array('id' => 'birthdayEdit', 'class' => 'form-control input-sm', 'name' => 'birthday', 'placeholder' => 'DD-MM-YYYY'),  $birthday);
									echo '</div></div>';

									$options = array(
									        'single'         => 'Single',
									        'inrelationship' => 'In relationship',
									        'engaged'        => 'Engaged',
									        'married'        => 'Married',
									);
									echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Relationship</label><div class="col-sm-10">';
									echo form_dropdown(array('id' => 'realationshipEdit', 'class' => 'input-sm btn-block', 'name' => 'relationship'), $options, $relationship);
									echo '</div></div>';
								
									echo '<div class="alert alert-danger" id="error">'.validation_errors().'</div>';

									echo '<div class="form-group"><div class="col-sm-offset-2 col-sm-10">';
									echo form_submit(array('id' => 'editProfile', 'class' => 'btn btn-info btn-sm profile-settings'), 'Save');
									echo '</div></div>';

									echo form_close();
					  		?>	
					  		</div>  
						</div><!--Panel Body-->

   					</div><!--Panel Default-->
  

					<div class="panel panel-default tab-content" id="tab-3">
				  		<div class="panel-heading grid-panel-title">
							<h3 class="panel-title titleBar" data-title="Password">Password</h3>
				  		</div>
				  
		   				<div class="panel-body">  
					  		<?php
						    	echo form_open('/user/update_password', array('class' => 'form-horizontal')); 
									
								echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Current password</label>';
								echo form_password(array('class' => 'form-control input-sm', 'name' => 'password', 'placeholder' => 'Current password'));
								echo '</div>';
							
								echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">New password</label>';
								echo form_password(array('class' => 'form-control input-sm', 'name' => 'npassword', 'placeholder' => 'New password'));
								echo '</div>';

								echo '<div class="form-group"><label class="col-sm-2 control-label input-sm">Confirm password</label>';
								echo form_password(array('class' => 'form-control input-sm', 'name' => 'cpassword', 'placeholder' => 'Confirm password'));
								echo '</div>';

								echo '<div class="alert alert-danger" id="error" role="alert">'.validation_errors().'</div>';

								echo '<div class="form-group"><div class="col-sm-offset-2 col-sm-10">';
								echo form_submit(array('id' => 'editProfile', 'class' => 'btn btn-info btn-sm profile-settings-password'), 'Save');
								echo '</div></div>';

								echo form_close();
					  		?>
						</div><!--End of Panel Body-->
				   </div><!--End of Panel Default-->
		 		</div><!--End of col-md-*-->
		  	</div><!--End of Row-->
			<div class="clearfix visible-xs-block"></div>
		</div><!--End of Container-->
		<footer class="clearfix footer-grid col-md-12">
        <p class="footer-p">
    		<a class="link-footer" href="">About Us</a> 
    		<a class="link-footer" href="">Help</a>
    		<a class="link-footer" href="">Terms of Service</a>

	    	© 2015 Ngariung
	    </p>
      </footer>
	</body>
</html>