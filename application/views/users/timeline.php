<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/head');?>
  </head>
    
  <body>
     <?php echo $this->parser->parse('template/navigation_bar', array('img_user' => base_url('assets/img/profile').'/'.$me['filename'], 'name' => $me['name']), TRUE)?>
      <!--container-->  
      <div class="container col-md-12">
        <div class="row col-pb"> 
          <div class="col-md-4">
            <?php echo $this->parser->parse('template/profile_banner', array('name' => $me['name'],
                                                                    'username' => $me['username'],
                                                                    'email' => $me['email'],
                                                                    'work' => $me['work'],
                                                                    'relationship' => $me['relationship'],
                                                                    'birthday' => $me['birthday'],
                                                                    'img_user' => base_url('assets/img/profile').'/'.$me['filename']
                                                                    ), TRUE);?>
          </div><!--end of col-md-*-->
      
        <div class="col-md-8">
          <?php 
          if(!empty($this->session->flashdata('confirm')['success'])){
            echo '<div class="alert alert-success" id="success_signin" role="alert">'.$this->session->flashdata('confirm')['success'].'</div>';
          }
          if(!empty($this->session->flashdata('confirm')['error'])){
            echo '<div class="alert alert-danger" id="error" role="error">'.$this->session->flashdata('confirm')['error'].'</div>';
          }
        ?>            <div id="timeline" class="panel panel-default">
              <div class="panel-heading grid-panel-title">
                <h3 class="panel-title titleBar" data-title="Profile">Timeline</h3>
              </div>
      
            <div class="panel-body">
              <?php 
                if(!empty($users))
                {
                  foreach($users as $user)
                  {
                    echo $this->parser->parse('template/user_profile_item',array('img_user' => base_url('assets/img/profile').'/'.$user['filename'],
                                                                          'name' => $user['name'],
                                                                          'username' => $user['username'],
                                                                          'email' => $user['email']
                                                                          ), TRUE);
                  }
                  
                }
                else
                  echo 'There is no result!';
              ?>
              <div class="pagination">
                <ul>
                  <?php echo $this->pagination->create_links();?>
                </ul>
          </div>

            </div><!--End of Panel Body-->
          </div><!--End of Panel Default-->
        </div><!--End of col-md-*-->
      </div><!--End of Row-->
    <div class="clearfix visible-xs-block"></div>
    </div><!--End of Container-->
    <footer class="clearfix footer-grid col-md-12">
        <p class="footer-p">
        <a class="link-footer" href="">About Us</a> 
        <a class="link-footer" href="">Help</a>
        <a class="link-footer" href="">Terms of Service</a>

        © 2015 Ngariung
      </p>
      </footer>
  </body>
</html>