<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploader extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                // load model
                $this->load->model('image_model');
                $this->load->model('user_model');
        }

        public function index()
        {
                
        }

        public function do_upload()
        {
                $config['upload_path']          = 'assets/img/profile/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000;
                $config['max_width']            = 1500;
                $config['max_height']           = 1000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data =  $this->upload->data();

                        $file_id = $this->image_model->insert($data['file_name']);
                        if($file_id)
                        {
                                $this->user_model->update(array('img_id' => $file_id), $this->session->userdata('user_id'));
                                $msg = "File successfully uploaded";
                        }
                        
                }
                redirect('');
        }
}
?>