<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		// load libraries
		$this->load->library('pagination');

		// load model
		$this->load->model('user_model');
	}
	public function index()
	{
		if($this->session->userdata('is_user_logged_in'))
		{	
			$config['base_url'] = base_url();
			$config['total_rows'] = $this->user_model->count_all();
			$config['per_page'] = 10; 		

			// customize pagination
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$config['cur_tag_open'] = '<li class="active"><a>';
			$config['cur_tag_close'] = '</a></li>';

			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';

			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';

			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';

			$config['prev_tag_open'] = '<li class="previous">';
			$config['prev_tag_close'] = '</li>';

			
			$offset = $this->uri->segment('1');
			$usersdata= $this->user_model->get_data_many($config['per_page'], $offset);
			$this->pagination->initialize($config); 

			$mydata = $this->user_model->get_data_mine();

			$data = array('users' => $usersdata, 'me' => $mydata ); 
			$this->load->view('users/timeline', $data);
		}
		else
		{
			$this->load->view('home');
		}
	}

	public function signup()
	{
		if(!($this->session->userdata('is_user_logged_in')))
		{
			$this->load->view('users/signup');
		}
		else
		{
			redirect('');
		}
	}

	public function setting()
	{
		if($this->session->userdata('is_user_logged_in'))
		{
			$mydata = $this->user_model->get_data_mine();		 
			$this->load->view('users/setting', $mydata);
		}
		else
		{
			redirect('');
		}
	}

	public function admin()
	{
		if($this->session->userdata('is_admin_logged_in'))
		{
			$this->load->model('user_model');
			$users = $this->user_model->get_data_many(10);
			$data = array('users' => $users);
			$this->load->view('admin/dashboard', $data);
		}
		else
		{
			$this->load->view('admin/login');
		}
	}

	public function search()
	{
		if($this->session->userdata('is_user_logged_in'))
		{
			$this->load->view('users/search');
		}
		else
		{
			$this->load->view('users/login');
		}
	}

	public function users_manager()
	{
		$this->load->model('user_model');
		$userdata = $this->user_model->get_data($this->input->get('user'));
		$this->load->view('admin/view_user', $userdata);
	}
}

?>
