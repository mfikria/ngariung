<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	//Constructing Users
	public function __construct()
	{
		parent::__construct();

		// load library
		$this->load->library('table');
		$this->load->library('form_validation');

		// load model
		$this->load->model('user_model');
	}

	// Indexing users data
	public function index()
	{
 
	}

	public function update_setting()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[20]|xss_clean');
	
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|xss_clean');
		
		if($this->form_validation->run())
		{
			if ($this->user_model->update(array('username' => $this->input->post('username'), 'email' => $this->input->post('email')), $this->session->userdata('user_id')))
			{
				$confirm['success'] = 'Update basic information success!.';
				$this->session->set_flashdata('confirm',$confirm);
			}
			redirect('');
		}
	}

	public function update_profile()
	{	
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]|xss_clean');
		
		$this->form_validation->set_rules('relationship', 'Relationship', 'required');

		$this->form_validation->set_rules('work', 'Work');

		$this->form_validation->set_rules('birthday', 'Birthday');
		
		if($this->form_validation->run())
		{
			if ($this->user_model->update(array('name' => $this->input->post('name'),
														'relationship'=> $this->input->post('relationship'),
														'work' => $this->input->post('work'),
														'birthday' => $this->input->post('birthday')
														), $this->session->userdata('user_id')))
			{
				$confirm['success'] = 'Update profile information success!.';
				$this->session->set_flashdata('confirm', $confirm);
			}
			redirect('');
		}
	}

	public function update_password()
	{
			
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]');

		$this->form_validation->set_rules('npassword', 'New Password', 'required|trim|min_length[5]');
		
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|min_length[5]|matches[npassword]');
		
		if($this->form_validation->run() && 
			$this->user_model->validate_password($this->session->userdata('user_id'), $this->input->post('password')))
		{
			if ($this->user_model->update(array('password' => md5($this->input->post('npassword'))), $this->session->userdata('user_id')))
			{
				$confirm['success'] = 'Update password success!.';
				$this->session->set_flashdata('confirm',$confirm);
			}
			redirect('');
		}	
	}

	public function search()
	{
		if(null != $this->input->get('q'))
		{
			$mydata = $this->user_model->get_data_mine();
			$usersdata = $this->user_model->search($this->input->get('q'));
			if(empty($usersdata))
			{
				$confirm['error'] = 'Try Again! Email or password is invalid.';
				$this->session->set_flashdata('confirm',$confirm);
			}
			$data = array('users' => $usersdata, 'me' => $mydata ); 

			$this->load->view('users/search', $data);
		}
		else
		{
			echo 'Insert what you want to search!';
		}
	}

	public function login_validation() 
	{	
		$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email|callback_validate_credentials');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|md5');			
		if($this->form_validation->run())
		{
			$data = array(
				'user_id' => $this->user_model->get_id(array('email' => $this->input->post('email'))),
				'is_user_logged_in' => 1
			);
			$this->session->set_userdata($data);

		}
		$confirm['error'] = 'Try Again! Email or password is invalid.';
		$this->session->set_flashdata('confirm',$confirm);
		redirect('');
	}
	
	public function validate_credentials()
	{
		if($this->user_model->validate())
		{
			return true;	
		}
		else
		{
			$this->form_validation->set_message('validate_credentials', 'Incorrect username/password.');
			return false;
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}

	public function signup_validation()
	{
		$this->load->library('form_validation');
		$this->load->model('user_model');
		
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]|xss_clean');
	
		$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]|max_length[20]|xss_clean');
		
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]');
		
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|min_length[5]|matches[password]');
		
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]|xss_clean');
		
		$this->form_validation->set_rules('relationship', 'Relationship', 'required');
		
		$this->form_validation->set_message('is_unique', 'That email was already existed');
		
		if($this->form_validation->run())
		{
			if ($this->user_model->add(array('name' => $this->input->post('name'),
												'relationship'=> $this->input->post('relationship'),
												'username' => $this->input->post('username'),
												'email' => $this->input->post('email'),
												'password' => md5($this->input->post('password'))
												)))
			{
				$confirm['success'] = 'Signup success! You can login now!';
				

			}
		}else
		{
			$confirm['error'] = 'Invalid. Check your input data again!';
		}
		$this->session->set_flashdata('confirm',$confirm);
		redirect('');
		
		
	}

}

?>
