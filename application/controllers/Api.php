<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		// load model
		$this->load->model('user_model');
	}

	public function get_info()
	{
		$username = $this->input->post('username');

		if(!empty($username))
		{
			$user_id = $this->user_model->get_id(array('username' => $username));
			$userdata = $this->user_model->get_data($user_id);
			if(empty($userdata))
			{
				$this->output
				->set_status_header('404')
				->set_output('The user data cannot be found');
			}
			else
			{
				$this->output
					->set_content_type('application/json')
					->set_output(json_encode($userdata));	
			}	

		}
		else
		{
			$this->output
				->set_status_header('404')
				->set_output('The user data cannot be found');
		}
	}
}
?>
