<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    
        // load model
        $this->load->model('admin_model');
        $this->load->model('user_model');
    	
    	// load library
    	$this->load->library('table');
    	$this->load->library('pagination');
    	$this->load->library('encrypt');
    	$this->load->library('form_validation');

    }

	public function index()
	{
		if($this->session->userdata('is_admin_logged_in'))
		{
			$this->view_user();
		}
		else
		{
			$this->login();
		}
	}

	public function login()
	{
		$this->load->view('admin/login');
	}

	public function login_validation() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('adminname', 'Adminname', 'required|trim|xss_clean|callback_validate_credentials');
		
		$this->form_validation->set_rules('password', 'Password', 'required|trim|md5');
		echo  $this->input->post('adminname');
		echo  $this->input->post('password');
			
		if($this->form_validation->run()) 
		{
			$data = array(
				'admin_id' => $this->admin_model->get_id(array('adminname' => $this->input->post('adminname'))),
				'is_admin_logged_in' => 1
			);
			$this->session->set_userdata($data);
		}
		redirect('admin');
	}
	
	public function validate_credentials()
	{	
			if($this->admin_model->validate())
			{
				return true;	
			}
			else
			{
				$this->form_validation->set_message('validate_credentials', 'Incorrect username/password.');
				return false;
			}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		$this->login();
	}

	public function delete_user($user_id)
	{
		$this->user_model->delete($user_id);
		$confirm['success'] = 'Deleting user success!.';
		$this->session->set_flashdata('confirm',$confirm);
		redirect('admin');
	}

	public function edit_user($user_id)
	{

		$userdata = $this->user_model->get_data($user_id);

		$admindata = $this->admin_model->get_data($this->session->userdata('admin_id'));

		$data = array('user' => $userdata, 'admin' => $admindata);

		$this->load->view('admin/edit_user', $data);
	}

	public function update_user($user_id)
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[20]|xss_clean');
	
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|xss_clean');
		
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]|xss_clean');
		
		$this->form_validation->set_rules('relationship', 'Relationship', 'required');

		$this->form_validation->set_rules('work', 'Work');

		$this->form_validation->set_rules('birthday', 'Birthday');

		
		
		if($this->form_validation->run())
		{
			if ($this->user_model->update(array('name' => $this->input->post('name'),
												'relationship'=> $this->input->post('relationship'),
												'work' => $this->input->post('work'),
												'birthday' => $this->input->post('birthday'),
												'username' => $this->input->post('username'),
												'email' => $this->input->post('email')
												),
											$user_id
										)
								)				
			{
				$confirm['success'] = 'Updating user\'s information success!.';
				$this->session->set_flashdata('confirm', $confirm);
			}
			
		}
		redirect('admin');
	}

	public function view_user()
	{
		$config['base_url'] = base_url().'/admin';
		$config['total_rows'] = $this->user_model->count_all();
		$config['per_page'] = 10; 		

		// customize pagination
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';

		$config['prev_tag_open'] = '<li class="previous">';
		$config['prev_tag_close'] = '</li>';

		$offset = $this->uri->segment('2');
		$usersdata= $this->user_model->get_data_many($config['per_page'], $offset);
		$this->pagination->initialize($config); 

		$admindata = $this->admin_model->get_data($this->session->userdata('admin_id'));

		$data = array('users' => $usersdata, 'admin' => $admindata); 
		$this->load->view('admin/dashboard', $data);
	}

	public function add_user()
	{
		$admindata = $this->admin_model->get_data($this->session->userdata('admin_id'));

		$data = array('admin' => $admindata);
		$this->load->view('admin/add_user', $data);
	}

	public function add_user_validation()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]|max_length[20]|xss_clean');
	
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]|xss_clean');
		
		$this->form_validation->set_message('is_unique', 'That username was already existed');
		
		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[50]|xss_clean');
		
		$this->form_validation->set_rules('relationship', 'Relationship', 'required');

		$this->form_validation->set_rules('work', 'Work');

		$this->form_validation->set_rules('birthday', 'Birthday');

		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]');

		
		if($this->form_validation->run())
		{
			if ($this->user_model->add(array('name' => $this->input->post('name'),
												'relationship'=> $this->input->post('relationship'),
												'work' => $this->input->post('work'),
												'birthday' => $this->input->post('birthday'),
												'password' => md5($this->input->post('password')),
												'username' => $this->input->post('username'),
												'email' => $this->input->post('email')
												),
											$user_id
										)
								)					
			{
				$confirm['success'] = 'Adding user success!';
				$this->session->set_flashdata('confirm',$confirm);
			}
			
		}
		redirect('admin');
	}
}

?>
