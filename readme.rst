##Panduan Deploy ke Server

1. Upload semua files dan folder pada folder Src ke public_html atau www pada root directory atau pada sub-directory
2. Buka file ../application/config/config.php, lalu lakukan perubahan pada beberapa variable
# Change this...
$config['base_url'] = 'http://your_application_directory';

3. Buka file ../application/config/database.php, lalu lakukan berikut ini untuk menyesuaikan pada environment database yang ada
# These lines should be changed...
'hostname' => 'database_host',
'username' => 'database_user',
'password' => 'database_password',
'database' => 'database_name',

4. Lakukan mod_rewrite untuk mengabaikan file index pada routing address dengan mengubah file .htaccess pada root directory. Ubah application_folder dengan folder tempat aplikasi kita berada. Jika aplikasi berada pada root directory isikan kosong ''.
# These line should be changed...
RewriteBase /applicaton_folder/

5. Untuk database SQL, silakan import database calonra_ngariung.sql yang ada pada folder deliverable. Adapun, tabel yang diperlukan adalah tabel users, admins, dan images

##Panduan Akses Web
Untuk login menjadi administrator silakan menuju link http://calonra.tk/m_fikria/admin dengan test case sebagai berikut
adminname: admin
password: 12345

Sementara untuk login sebagai user dapat langsung dilakukan di halaman utama http://calonra.tk/m_fikria

##Panduan Akses API

Untuk mengakses API dapat dilakukan dengan mengirim request ke http://calonra.tk/m_fikria/user/get_info dengan metode 'POST' yang mengirimkan parameter berupa key 'username' lalu server akan mengirimkan data user yang bersangkutan dengan format .json